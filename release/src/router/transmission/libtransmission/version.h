#define PEERID_PREFIX             "-TR2220-"
#define USERAGENT_PREFIX          "2.22"
#define SVN_REVISION              "12099"
#define SVN_REVISION_NUM          12099
#define SHORT_VERSION_STRING      "2.22"
#define LONG_VERSION_STRING       "2.22 (12099)"
#define VERSION_STRING_INFOPLIST  2.22
#define MAJOR_VERSION             2
#define MINOR_VERSION             22
#define TR_STABLE_RELEASE         1
